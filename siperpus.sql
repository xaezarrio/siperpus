-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Feb 14, 2023 at 08:01 AM
-- Server version: 10.4.22-MariaDB
-- PHP Version: 7.4.27

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `siperpus`
--

-- --------------------------------------------------------

--
-- Table structure for table `buku`
--

CREATE TABLE `buku` (
  `id` int(11) NOT NULL,
  `kode` varchar(8) NOT NULL,
  `jenis` int(11) NOT NULL,
  `judul` varchar(68) NOT NULL,
  `tanggal` date NOT NULL,
  `pengarang` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `buku`
--

INSERT INTO `buku` (`id`, `kode`, `jenis`, `judul`, `tanggal`, `pengarang`) VALUES
(1, 'SVJZGXVJ', 2, 'Ayat-Ayat Cinta', '2017-12-21', 'Habiburrahman El Shirazy'),
(2, 'KQKM6UJ5', 1, 'Ensiklopedia Komunikasi', '2015-04-03', 'Alex Sobur'),
(3, '0OCQBBQX', 4, 'Mari Bung Rebut Kembali', '2012-10-29', 'Willy Aditya'),
(4, 'ZR1PC9UI', 2, 'Rentang Kasih', '2017-09-03', 'Gita Savitri'),
(5, 'N2FXZ2GG', 1, 'MYSQL untuk pemula', '2023-02-12', 'Jason'),
(6, 'ZLNY2EMT', 2, 'Meraih Mimpi', '2023-02-13', 'Jonathan');

-- --------------------------------------------------------

--
-- Table structure for table `mahasiswa`
--

CREATE TABLE `mahasiswa` (
  `id` int(11) NOT NULL,
  `nim` int(11) DEFAULT NULL,
  `nama` varchar(45) CHARACTER SET latin1 DEFAULT NULL,
  `tempat_lahir` varchar(41) CHARACTER SET latin1 DEFAULT NULL,
  `tanggal_lahir` date DEFAULT NULL,
  `jurusan` varchar(30) CHARACTER SET latin1 DEFAULT NULL,
  `tanggal` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `mahasiswa`
--

INSERT INTO `mahasiswa` (`id`, `nim`, `nama`, `tempat_lahir`, `tanggal_lahir`, `jurusan`, `tanggal`) VALUES
(1, 14619, 'Suluh Sulistiawan', 'Pemalang', '2004-06-22', 'Teknik Komputer dan Jaringan', '2018-07-16'),
(3, 14617, 'Hamdan Ainur R.', 'Pemalang', '2004-06-22', 'Teknik Komputer dan Jaringan', '2018-07-16'),
(4, 20213, 'Jeffry Ho', 'Malang', '1994-01-02', 'Teknik Komputer dan Jaringan', '2021-11-01');

-- --------------------------------------------------------

--
-- Table structure for table `murid-`
--

CREATE TABLE `murid-` (
  `id` int(11) NOT NULL,
  `nim` int(5) NOT NULL,
  `nama` varchar(45) NOT NULL,
  `tempat_lahir` varchar(41) NOT NULL,
  `tanggal_lahir` date NOT NULL,
  `jurusan` varchar(30) NOT NULL,
  `tanggal` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `murid-`
--

INSERT INTO `murid-` (`id`, `nim`, `nama`, `tempat_lahir`, `tanggal_lahir`, `jurusan`, `tanggal`) VALUES
(1, 14619, 'Suluh Sulistiawan', 'Pemalang', '2004-06-22', 'Teknik Komputer dan Jaringan', '2018-07-16'),
(3, 14617, 'Hamdan Ainur R.', 'Pemalang', '2003-00-00', 'Teknik Komputer dan Jaringan', '2018-07-16'),
(4, 20213, 'Jeffry Ho', 'Malang', '1994-01-02', 'Teknik Komputer dan Jaringan', '2021-11-01');

-- --------------------------------------------------------

--
-- Table structure for table `peminjaman`
--

CREATE TABLE `peminjaman` (
  `id` int(11) NOT NULL,
  `nim` int(5) NOT NULL,
  `nama` varchar(45) NOT NULL,
  `jurusan` varchar(30) NOT NULL,
  `kode` varchar(8) NOT NULL,
  `judul` varchar(68) NOT NULL,
  `tanggal` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `peminjaman`
--

INSERT INTO `peminjaman` (`id`, `nim`, `nama`, `jurusan`, `kode`, `judul`, `tanggal`) VALUES
(1, 14619, 'Suluh Sulistiawan', 'Teknik Komputer dan Jaringan', 'SVJZGXVJ', 'Ayat-Ayat Cinta', '2019-12-03'),
(2, 14619, 'Suluh Sulistiawan', 'Teknik Komputer dan Jaringan', 'KQKM6UJ5', 'Ensiklopedia Komunikasi', '2019-12-03'),
(9, 14619, 'Suluh Sulistiawan', 'Teknik Komputer dan Jaringan', '0OCQBBQX', 'Mari Bung Rebut Kembali', '2023-02-10'),
(13, 14617, 'Hamdan Ainur R.', 'Teknik Komputer dan Jaringan', 'SVJZGXVJ', 'Ayat-Ayat Cinta', '2023-02-10'),
(14, 14617, 'Hamdan Ainur R.', 'Teknik Komputer dan Jaringan', '0OCQBBQX', 'Mari Bung Rebut Kembali', '2023-02-10'),
(16, 20213, 'Jeffry Ho', 'Teknik Komputer dan Jaringan', 'SVJZGXVJ', 'Ayat-Ayat Cinta', '2023-02-13');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `buku`
--
ALTER TABLE `buku`
  ADD PRIMARY KEY (`id`,`kode`) USING BTREE;

--
-- Indexes for table `mahasiswa`
--
ALTER TABLE `mahasiswa`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `murid-`
--
ALTER TABLE `murid-`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indexes for table `peminjaman`
--
ALTER TABLE `peminjaman`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `buku`
--
ALTER TABLE `buku`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `mahasiswa`
--
ALTER TABLE `mahasiswa`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `murid-`
--
ALTER TABLE `murid-`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `peminjaman`
--
ALTER TABLE `peminjaman`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
